#!/usr/bin/python
# -*- coding: utf-8 -*-

#tablero = '..3.2.6..9..3.5..1..18.64....81.29..7.......8..67.82....26.95..8..2.3..9..5.1.3..'
tablero = '.2..31.6.3.6..5..75.9.7.....3....6.5...12..3..67.8..19..83.....6..8..47....76....'
filas = 'ABCDEFGHI'
columnas = '123456789'
boxes = ''

#Genera el tablero A1,A2,A3.....I7,I8,I9
def cross(a, b):
      return [s+t for s in a for t in b]

#Tablebro original
def grid_values_original(boxes,grid):
    return dict(zip(boxes, grid))

#Tablebro Eliminacion de opciones
def grid_values(boxes, grid):
    values = []
    for c in grid:
        if c == '.':
            values.append('123456789')
        elif c in '123456789':
            values.append(c)
    return dict(zip(boxes, values))

#Pinta tablebro sudoku
def display(boxes, values):
    width = 1+max(len(values[s]) for s in boxes)
    line = '+'.join(['-'*(width*3)]*3)
    for r in filas:
        print(''.join(values[r+c].center(width)+('|' if c in '36' else '')
                      for c in columnas))
        if r in 'CF': print(line)

#Eliminar digitos no aptos
def eliminate(peers, values):
    solved_values = [box for box in values.keys() if len(values[box]) == 1]
    for box in solved_values:
        digit = values[box]
        for peer in peers[box]:
            values[peer] = values[peer].replace(digit,'')
    return values

def only_choice(unitlist, values):
    """
    Finalize all values that are the only choice for a unit.
    Go through all the units, and whenever there is a unit with a value
    that only fits in one box, assign the value to this box.
    Args: Sudoku in dictionary form.
    Returns: Resulting Sudoku in dictionary form after filling in only choices.
    """
    for unit in unitlist:
        for digit in '123456789':
            dplaces = [box for box in unit if digit in values[box]]
            if len(dplaces) == 1:
                values[dplaces[0]] = digit
    return values

def reduce_puzzle(peers, unitlist, values):
    """
    Use Eliminate Strategy, Only Choice Strategy, & Naked Twins Strategy
    Args: Sudoku in dictionary form.
    Returns: Resulting Sudoku in dictionary form after eliminating invalid
             choice and filling in only choices.
    """
    stalled = False
    while not stalled:
        # Check how many boxes have a determined value
        solved_values_before = len([box for box in values.keys() if len(values[box]) == 1])
        # se the Eliminate Strategy
        values = eliminate(peers,values)
        # Use the Only Choice Strategy
        values = only_choice(unitlist, values)
        # Check how many boxes have a determined value, to compare
        solved_values_after = len([box for box in values.keys() if len(values[box]) == 1])
        # If no new values were added, stop the loop.
        stalled = solved_values_before == solved_values_after
        # Sanity check, return False if there is a box with zero available values:
        if len([box for box in values.keys() if len(values[box]) == 0]):
            return False
    return values

def main():
    boxes = cross(filas, columnas)
    #display(boxes, grid_values_original(boxes,tablero))
    #display(boxes,grid_values(boxes, tablero))
    row_units = [cross(r, columnas) for r in filas]
    column_units = [cross(filas, c) for c in columnas]
    square_units = [cross(rs, cs) for rs in ('ABC','DEF','GHI') for cs in ('123','456','789')]
    unitlist = row_units + column_units + square_units
    units = dict((s, [u for u in unitlist if s in u]) for s in boxes)
    peers = dict((s, set(sum(units[s],[]))-set([s])) for s in boxes)
    print("")
    #display(boxes,eliminate(peers,grid_values(boxes,tablero)))
    display(boxes, reduce_puzzle(peers,unitlist,grid_values(boxes, tablero)))

if __name__ == "__main__":
    main()